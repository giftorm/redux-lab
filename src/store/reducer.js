import *  as actionTypes from './actions'

const initialState = {
    counter: 0,
    results: []
};

const reducer = (state = initialState, action) => {
    switch( action.type ) {
        case actionTypes.INCREMENT:
            return { ...state, counter: state.counter + 1 };
        case actionTypes.DECREMENT:
            return { ...state, counter: state.counter - 1 };
        case actionTypes.ADD:
            return { ...state, counter: state.counter + 10 };
        case actionTypes.SUBTRACT:
            return { ...state, counter: state.counter - 15 };
        case actionTypes.STORE_RESULT:
            return { ...state, results: state.results.concat({id: new Date(), value: state.counter}) }; // concat wont affect the old state like push
        case actionTypes.DELETE_RESULT:
            // const id = 2;
            // const newArray = [...state.results];
            // newArray.splice(id, 1);
            // Delete object immutably
            const updatedArray = state.results.filter(result => result.id !== action.resultElementId); // Return array object which doesnt match to id
            return { ...state, results: updatedArray };
        default:
    }
    return state;
};



export default reducer;