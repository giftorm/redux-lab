import *  as actionTypes from '../actions'

const initialState = {
    results: []
};

const reducer = (state = initialState, action) => {
    switch( action.type ) {
        case actionTypes.STORE_RESULT:
            console.log(new Date());
            
            return { ...state, results: state.results.concat({id: new Date(), value: action.result}) }; // concat wont affect the old state like push
        case actionTypes.DELETE_RESULT:
            // const id = 2;
            // const newArray = [...state.results];
            // newArray.splice(id, 1);
            // Delete object immutably
            const updatedArray = state.results.filter(result => result.id !== action.resultElementId); // Return array object which doesnt match to id
            return { ...state, results: updatedArray };
        default:
    }
    return state;
};



export default reducer;