import * as actionTypes from './actions';

const initialState = {
    persons: []
};

const reducer = (state = initialState, action) => {
    switch( action.type ) {
        case actionTypes.ADD:
        const newPerson = {
            key: Math.random(),
            name: action.personData.name,
            age: action.personData.age
        }
            return { ...state, persons: state.persons.concat(newPerson) };
        case actionTypes.DELETE:
            const updatedArray = state.persons.filter(person => person.key !== action.key)
            return { ...state, persons: updatedArray };
        default:
    }
    return state;
};

export default reducer;